class BrowserManager:
    def __init__(self, playwright):
        self.playwright = playwright

    def RunBrowser(self, browserType = 'chrome'):
        match browserType:
            case 'firefox':
                browser = self.playwright.firefox
            case _:
                browser = self.playwright.chromium.launch()
        return browser

    def Close(self, item):
        item.close()

    def NewPage(self, context):
        page = context.new_page()
        return page

    def createContext(self, browser):
        context = browser.new_context()
        return context