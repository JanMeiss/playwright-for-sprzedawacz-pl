from Classes.BrowserManager import BrowserManager


class TestInit:

    def __init__(self, playwright):
        self.page = None
        self.context = None
        self.browser = None
        self.manager = BrowserManager(playwright)

    def Initialize(self, browserType='chrome'):
        self.browser = self.manager.RunBrowser(browserType)
        self.context = self.manager.createContext(self.browser)
        self.page = self.manager.NewPage(self.context)

        return self.page


    def Finalize(self):
        self.manager.Close(self.browser)
        self.manager.Close(self.context)
        self.manager.Close(self.page)