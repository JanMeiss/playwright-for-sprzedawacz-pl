from InputData import InputData

class LoginPage:
    def __init__(self, page):
        """

        :type page: object
        """
        self.page = page

    def FillField(self, selector, value):
        self.WaitForSelector(selector)
        self.page.fill(selector, value)

    def WaitForSelector(self, selector):
        return self.page.wait_for_selector(selector)

    def ClickButton(self, selector):
        self.page.wait_for_selector(selector, state='visible')
        login_button = self.page.query_selector(selector)
        login_button.click()

    def Login(self, page, login, password):
        loginPage = LoginPage(page)
        loginPage.FillField(InputData.LoginSelector, login)
        loginPage.FillField(InputData.PasswordSelector, password)
        loginPage.ClickButton(InputData.ButtonSelector)
