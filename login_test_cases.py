import pytest
from Classes.LoginPage import LoginPage
from InputData import InputData
from Classes.TestInit import TestInit

@pytest.fixture(scope="module")
def page_instance(playwright):
    init = TestInit(playwright)
    page = init.Initialize()
    yield page
    init.Finalize()

@pytest.fixture()
def page(page_instance):
    page_instance.goto(InputData.LoginUrl)
    return page_instance

def test_incorrect_login(page):
    loginPage = LoginPage(page)
    loginPage.Login(page, 'seleniumkillers@gmail.com', 'seleniumkillers123!@#')
    selector_element = loginPage.WaitForSelector(".col-12")
    actual_text = selector_element.inner_text()
    assert actual_text == 'Wprowadzono błędny login lub hasło'

# empty email field
def test_empty_email_field(page):
    loginPage = LoginPage(page)
    loginPage.Login(page, '', '')
    current_url = page.url
    assert current_url == "https://sprzedawacz.pl/zaloguj/"